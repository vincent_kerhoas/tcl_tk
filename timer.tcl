#! /usr/bin/wish
########################################################################
# 							UI 
########################################################################

button .start -text Start -command Start
label  .time -textvar time -width 9 -bg black -fg green 
set time 00:00.00
button .stop -text Stop -command Stop
button .reset -text Reset -command Reset
eval pack [winfo children .] -side left -fill y

########################################################################
# 							PROC
########################################################################

proc every {ms body} {
	eval $body; after $ms [info level 0]
}

proc Start {} {
    if {$::time=="00:00.00"} {
        set ::time0 [clock clicks -milliseconds]
    }
    every 10 {
        set m [expr {[clock clicks -milliseconds] - $::time0}]
        set ::time [format %2.2d:%2.2d.%2.2d \
            [expr {$m/60000}] [expr {($m/1000)%60}] [expr {$m%1000/10}]]
    }
    .start config -state disabled
}

proc Stop {} {
    if {[llength [after info]]} {
        after cancel [after info]
    } else {set ::time 00:00.00}
    .start config -state normal
}

proc Reset {} {
	after cancel [after info]
	set ::time 00:00.00
    .start config -state normal
}
########################################################################
