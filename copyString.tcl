#! /usr/bin/wish
########################################################################
# 							UI 
########################################################################

wm title . "Copy String"
label .labelSource -text "Source"
entry .sourceEnt -width 25 -textvariable source
button .copyButton -text copy
label .labelDestination -text "Destination"
entry .destinationEnt -width 25 -textvariable destination

grid .labelSource .sourceEnt  -row 0  -pady 4 -sticky e
grid .copyButton  -row 1 -pady 4  -sticky e
grid .labelDestination .destinationEnt  -row 2  -pady 4 -sticky e

bind .copyButton <ButtonPress-1> copySource

########################################################################
# 							PROC
########################################################################

proc copySource {} {
	.destinationEnt delete 0 end
	
	# Write in terminal	:	
	puts "copy button pressed"	
	
	# [] : Interpolation (instructions imbriquées) :
	.destinationEnt insert 0 [ .sourceEnt get ] 

	.sourceEnt delete 0 end
}

########################################################################
