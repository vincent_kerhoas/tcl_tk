#! /usr/bin/wish

set SerialPort /dev/ttyACM0
set SerialMode "115200,n,8,1"

set chan [open $SerialPort r+]
fconfigure $chan -mode $SerialMode -translation binary -buffering none -blocking 0
fileevent $chan readable [list receiveMessage $chan]

########################################################################
# 							UI 
########################################################################
wm title . "Serial ihm"
label .labelToSendMes -text "Message to Send"
entry .toSendEntry -width 25 
button .sendMessageButton -text Send 
label .labelReceivedMes -text "Received Message"
entry .receivedEntry -width 25 
pack .labelToSendMes .toSendEntry .sendMessageButton .labelReceivedMes .receivedEntry
bind .sendMessageButton <ButtonPress-1> [list sendMessage $chan]

########################################################################
# 							PROC
########################################################################
proc sendMessage {chan} {

	puts $chan [ .toSendEntry get ]
	flush $chan
	.toSendEntry delete 0 end
}
 
proc receiveMessage {chan} {

     set data [read $chan]
     set size [string length $data]
     puts "received $size bytes: $data"
     .receivedEntry delete 0 end
     .receivedEntry insert @0 $data    
}
########################################################################
